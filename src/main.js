// Parse EAN13
EAN13 = document.querySelector("meta[itemprop='productID']").getAttribute("content");
console.log(EAN13)

// Load JsBarcode
var DOM_img = document.createElement("img");
DOM_img.id = "barcode"
document.body.appendChild(DOM_img);

// Append Barcode
JsBarcode("#barcode", EAN13, {
    format: "EAN13",
    margin: 3,
    lastChar: " ",
    height: 50
});